# -*- coding: utf-8 -*-
# try something like

adminEmails=[]
admins = db(db.roles.Role == "Administrator" or db.roles.Role == "Faculty Hiring Committee Chair").select()
for i in admins:
    adminEmails.append(i.Email_Id)

staffEmails=[]
staffs = db(db.roles.Role == "Faculty Hiring Committee Member" or db.roles.Role == "Faculty Member" or db.roles.Role == "Staff Member").select()
for i in staffs:
    staffEmails.append(i.Email_Id)

#Controller to manage Employee/Faculty Interface
def index():#Nurendra: Takes care of the forwarding of applications and viewing
    if request.vars.ticket and not session.flag == 1:
        session.token = CAS.login(request)
    if session.token != None:
        session.flag = 1
    if session.token in adminEmails:
        redirect(URL(c='facultyAdmin',f='index'))
    if session.flag != 1 or (session.token not in staffEmails):
        redirect(URL(c='employee',f='logout'))
    if len(request.args)>0:
        a = forwardToAdmin(request.args[0])
        if a is True:
            response.flash = T("Application Forwarded to the Admin")
        else:
            response.flash = T("Application has already been forwarded.")
    rows = db(db.auth_user.id>0).select(orderby=~db.auth_user.Time)
    return dict(rows = rows)

def viewFullApplication():#Nurendra: To see the full application of a candidate
    if request.vars.ticket and not session.flag == 1:
        session.token = CAS.login(request)
    if session.token != None:
        session.flag = 1
    if session.flag != 1:
        redirect(URL(c='employee',f='login'))
    if len(request.args) <= 0:
        redirect(URL('default','index'))
        return dict()
    appId = request.args[0]
    row = db(db.auth_user.id == appId).select()
    return dict(row = row)

def forwardToAdmin(appId):#Nurendra: To forward to admin
    if request.vars.ticket and not session.flag == 1:
        session.token = CAS.login(request)
    if session.token != None:
        session.flag = 1
    if session.flag != 1:
        redirect(URL(c='employee',f='login'))
    if len(db(db.forwardedToAdmin.applicationId == appId).select()) < 1:
        if db.forwardedToAdmin.insert(applicationId=appId):
            return True
    return False

####Nurendra: CAS Login for Faculty. Configured only for this application. The same code
####cannot be used elsewhere
CAS.login_url='https://login.iiit.ac.in/cas/login'
CAS.check_url='https://login.iiit.ac.in/cas/validate'
CAS.logout_url='https://login.iiit.ac.in/cas/logout'
CAS.my_url='http://hiring.iiit.ac.in/FacultyHiringPortal/employee/index'
try:
    session.flag
except:
    session.flag = 0
try:
    session.token
except:
    session.token = None

def login():
    return CAS.login()

def logout():
    session.flag = 0
    session.token = None
    return CAS.logout()
