# -*- coding: utf-8 -*-
# try something like

#Controller to handle all methods of a Admin.
adminEmails=[]
admins = db(db.roles.Role == "Administrator" or db.roles.Role == "Faculty Hiring Committee Chair").select()
for i in admins:
    adminEmails.append(i.Email_Id)

staffEmails=[]
staffs = db(db.roles.Role == "Faculty Hiring Committee Member" or db.roles.Role == "Faculty Member" or db.roles.Role == "Staff Member").select()
for i in staffs:
    staffEmails.append(i.Email_Id)

def index():#Nurendra: Interface to view application
    if request.vars.ticket and not session.flag == 1:
        session.token = CAS.login(request)
    if session.token != None:
        session.flag = 1
    if (session.flag == 1) and (session.token in staffEmails):
        redirect(URL(c='employee',f='index'))
    if session.flag != 1 or (session.token not in adminEmails):
        redirect(URL(c='facultyAdmin',f='login'))
    if True:
        return dict(message="")
    else:
        return dict(message="Nothing for you here :).")

def viewAllApplications():#Nurendra: To view all applications
    if session.flag != 1 or (session.token not in adminEmails):
        redirect(URL(c='facultyAdmin',f='login'))
    rows = db(db.auth_user.id>0).select(orderby=~db.auth_user.Time)
    return dict(rows = rows)

def viewForwardedApplications():
    if session.flag != 1 or (session.token not in adminEmails):
        redirect(URL(c='facultyAdmin',f='login'))
    forwards = db(db.forwardedToAdmin.id > 0).select()
    return dict(forwards = forwards)

def addRoles():
    if session.flag != 1 or (session.token not in adminEmails):
        redirect(URL(c='facultyAdmin',f='login'))
    form = SQLFORM(db.roles)
    check_form = SQLFORM.factory(
                 Field('Email','string',length=100,requires=IS_EMAIL()))
    role=None
    if form.process().accepted:
        response.flash = "Role is added"
    elif form.errors:
        response.flash = "Check for errors. Role is not added."
    if check_form.process().accepted:
        row = db(db.roles.Email_Id==check_form.vars.Email).select()
        if len(row) > 0:
            role = row[0]
        else:
            response.flash = "Role not present."
    elif check_form.errors:
        response.flash = "Enter valid Email"
    return dict(form=form,check_form=check_form,role=role)

####Nurendra: CAS Login for FacultyAdmin. Configured only for this application. The same code
####cannot be used elsewhere

CAS.login_url='https://login.iiit.ac.in/cas/login'
CAS.check_url='https://login.iiit.ac.in/cas/validate'
CAS.logout_url='https://login.iiit.ac.in/cas/logout'
CAS.my_url='http://hiring.iiit.ac.in/FacultyHiringPortal/facultyAdmin/index'
try:
    session.flag
except:
    session.flag = 0
try:
    session.token
except:
    session.token = None

def login():
    return CAS.login()

def logout():
    session.flag = 0
    session.token = None
    return CAS.logout()
