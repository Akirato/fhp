#-*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - api is an example of Hypermedia API support and access control
#########################################################################

from random import randint

def index():
    return dict()

#Nurendra: Function for generating passkey.
def passwd(a,b):
    i,j,keyword=0,0,''
    while i<len(a) and i<len(b):
        if j<5:
            keyword = keyword+str(randint(0,9))
            j=j+1
        keyword = keyword+a[i]+b[i]
        i+=1
    keyword = keyword + a[i:-1]+b[i:-1]+a[-1]+b[-1]
    keyword = ''.join(keyword.split())
    return keyword[::-1]

@auth.requires_login()
def optionalAddon():#Nurendra: controller takes a databases, converts it into form and submits it with validators
    row = db(db.optionalAddon.Username==auth.user.username).select()
    flag=1
    if len(row)>0:
        if row[0].login_flag==True:
            flag=0
    form=SQLFORM(db.optionalAddon)
    if form.process().accepted:
        session.flash = 'form accepted'
        db(db.optionalAddon.Username==auth.user.username).update(login_flag=True)
        row = db(db.optionalAddon.Username==auth.user.username).select()
        for i in row:
            if i.Recommendation_Email_1:
                keyword=passwd(i.Username,i.Recommendation_Name_1)
                mail2.send(to=i.Recommendation_Email_1, subject='Recommendation for '+auth.user.first_name+' '+auth.user.last_name+' applying for IIIT, Hyderabad', message='Dear '+i.Recommendation_Name_1 + ',\n' + auth.user.first_name + ' ' + auth.user.last_name + ' has applied for a Faculty position at International Institute of Information Technology, Hyderabad. Please share your views about him by logging in at ' + 'http://hiring.iiit.ac.in/FacultyHiringPortal/default/rlogin/'+ keyword + '\nPlease use:\n'+'Email: ' + i.Recommendation_Email_1 + '\n' + 'Passkey: '+keyword+'\nPlease send it within two weeks.\n'+'Regards,\n'+'Faculty Search,\nIIIT, Hyderabad\n'+'P.S: Please reply back to the email if you cannot relate to or do not understand the contents.\n')
                db.recommenders_login.insert(email=i.Recommendation_Email_1,passkey=keyword,login_count=False,recommendingfor=auth.user.username)
            if i.Recommendation_Email_2:
                keyword=passwd(i.Username,i.Recommendation_Name_2)
                mail2.send(to=i.Recommendation_Email_2, subject='Recommendation for '+auth.user.first_name+' '+auth.user.last_name+' applying for IIIT, Hyderabad', message='Dear '+i.Recommendation_Name_2 + ',\n' + auth.user.first_name + ' ' + auth.user.last_name + ' has applied for a Faculty position at International Institute of Information Technology, Hyderabad. Please share your views about him by logging in at ' + 'http://hiring.iiit.ac.in/FacultyHiringPortal/default/rlogin/'+ keyword + '\nPlease use:\n'+'Email: ' + i.Recommendation_Email_2 + '\n' + 'Passkey: '+keyword+'\nPlease send it within two weeks.\n'+'Regards,\n'+'Faculty Search,\nIIIT, Hyderabad\n'+'P.S: Please reply back to the email if you cannot relate to or do not understand the contents.\n')
                db.recommenders_login.insert(email=i.Recommendation_Email_2,passkey=keyword,login_count=False,recommendingfor=auth.user.username)
            if i.Recommendation_Email_3:
                keyword=passwd(i.Username,i.Recommendation_Name_3)
                mail2.send(to=i.Recommendation_Email_3, subject='Recommendation for '+auth.user.first_name+' '+auth.user.last_name+' applying for IIIT, Hyderabad', message='Dear '+i.Recommendation_Name_1 + ',\n' + auth.user.first_name + ' ' + auth.user.last_name + ' has applied for a Faculty position at International Institute of Information Technology, Hyderabad. Please share your views about him by logging in at ' + 'http://hiring.iiit.ac.in/FacultyHiringPortal/default/rlogin/'+ keyword + '\nPlease use:\n'+'Email: ' + i.Recommendation_Email_1 + '\n' + 'Passkey: '+keyword+'\nPlease send it within two weeks.\n'+'Regards,\n'+'Faculty Search,\nIIIT, Hyderabad\n'+'P.S: Please reply back to the email if you cannot relate to or do not understand the contents.\n')
                db.recommenders_login.insert(email=i.Recommendation_Email_3,passkey=keyword,login_count=False,recommendingfor=auth.user.username)
        db(db.auth_user.username==auth.user.username).update(Status=50)
        redirect(URL('index'))
    elif form.errors:
        response.flash = 'Form has errors'
    else:
        response.flash = 'Please fill the form'
    return dict(form=form,flag=flag)

def register():#sriram Register page Validation
    db.auth_user.registration_key.show_if=False
    db.auth_user.reset_password_key.show_if=False
    db.auth_user.registration_id.show_if=False
    db.auth_user.Level.show_if = False #Need to ensure Level Doesnt show up finally......need time to resolve/sriram
    form = SQLFORM(db.auth_user).process()
    return dict(form=form)

def recommendationPortal(): #subportal for recommenders
    if len(request.args)<=0:
        return dict(flag=1,form=0)
    row = db(db.recommenders_login.passkey==request.args[0]).select()
    checkEmail = row[0].email
    row2 = db(db.auth_user.username == row[0].recommendingfor).select()
    db.recommendationPage.Recommendation_forUsername.default=row2[0].username
    db.recommendationPage.Recommendation_For.default=row2[0].first_name+' '+row2[0].last_name
    db.recommendationPage.Email.default = checkEmail
    form = SQLFORM(db.recommendationPage)
    if form.process().accepted:
        db(db.recommenders_login.passkey == request.args[0]).delete()
        redirect(URL('default','success'))
    return dict(form = form,flag = 0)

@auth.requires_login()
def applicationStatus():#Nurendra: To show the application status of the logged in user
    optionalAddonflag = 0
    row = db(db.optionalAddon.Username == auth.user.username).select()
    if len(row) > 0:
        optionalAddonflag = 1
        return dict(flag = optionalAddonflag,row = row[0])
    return dict(flag = optionalAddonflag)

def success():
    return dict()

def rlogin():
    if len(request.args) == 1:
        row = db(db.recommenders_login.passkey == request.args[0]).select()
        form = SQLFORM.factory(Field('email', requires=[IS_NOT_EMPTY(), IS_EMAIL(error_message='Invalid Email')]),
                               Field('passkey', 'password', requires=[IS_NOT_EMPTY()])
                              )
        form.formstyle="bootstrap"
        if form.validate:
            if len(row)>0:
                if request.vars.email==row[0].email and request.vars.passkey==row[0].passkey:
                    redirect(URL(f='recommendationPortal',args=request.args[0]))
                else:
                    session.flash = 'Invalid Login'
            else:
                session.flash = 'Invalid Login'
        return dict(form = form,flag=1)
    else:
        form = 'You are not at the page You are supposed to be please go back'
        return dict(form = form,flag=0)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    form=auth()
    return dict(form = form)



def download():
    return response.stream(open(os.path.join(request.folder,'uploads',request.args[0]),'rb'))

#@cache.action()
#def download():
#    """
#    allows downloading of uploaded files
#    http://..../[app]/default/download/[filename]
#    """
#    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login()
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET':{},'POST':{},'PUT':{},'DELETE':{}},
        }
    return Collection(db).process(request,response,rules)

def faq():
    return dict()

def aboutus():
    return dict()


def contactUs():#Nurendra: Queries to be sent to <presently None>
    name = ''
    mailsd = ''
    if auth.is_logged_in():
        name = auth.user.first_name + ' '+auth.user.last_name
        mailsd = auth.user.email
    form = SQLFORM.factory(
        Field('Name', 'string', default=name, requires=IS_NOT_EMPTY()),
        Field('Email', 'string', default=mailsd, requires=IS_EMAIL(error_message='Invalid Email!')),
        Field('Doubts', 'text', requires=IS_NOT_EMPTY()),
        formstyle = 'bootstrap')
    if form.process().accepted:
        response.flash = 'Your Query has been successfully sent'
        mail2.send(to=None, subject='Query from IIIT-H Faculty Hiring Portal', message='Dear \n' + "Here's the query,\n Name: "+form.vars.Name+"\nEmail: "+form.vars.Email+"\nDoubts: "+form.vars.Doubts+'\nRegards,\n Faculty Hiring Portal, IIIT Hyderabad.\n')
    elif form.errors:
        response.flash = 'Query was not submitted. Check you form.'
    return dict(form=form)
