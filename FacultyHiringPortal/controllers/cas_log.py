# -*- coding: utf-8 -*-
# try something like
CAS.login_url='https://login.iiit.ac.in/cas/login'
CAS.check_url='https://login.iiit.ac.in/cas/validate'
CAS.logout_url='https://login.iiit.ac.in/cas/logout'
CAS.my_url='http://sites.iiit.ac.in/Survey/admin/login'

# ---------- HOME PAGE IS SAME FOR ALL THE USERS --------------------
if not session.token and not request.function=='login':
     redirect(URL(r=request, f='login'))
    
def login():
    session.login = 0
    session.token = CAS.login(request)
    if session.token and (session.token == "guruprasad.j@iiit.ac.in" or session.token=="priyanka.srivastava@iiit.ac.in" or session.token=="dipti@iiit.ac.in" or session.token=="pjn@iiit.ac.in" or session.token=="vivek.jain@students.iiit.ac.in"):
        session.login = 1
        redirect(URL(r=request,f='roll_no'))
    else:
        session.flash="Invalid login"
        redirect(URL(r=request,f='login'))
     
     
def logout():
    session.login = 0
    session.token=None
    CAS.logout()
