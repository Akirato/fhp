# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()
import os
if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('mysql://adminhiring:passhireresource@localhost/hiring') #Nurendra: database for candidate data.

else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore+ndb')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []

## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Service, PluginManager, Crud, prettydate

auth = Auth(db)
service = Service()
plugins = PluginManager()
crud= Crud(db)
register_extra_fields = [
        Field('Confirm_Password','password',requires=IS_EXPR('value==%s' % repr(request.vars.get('password', None)),
                 error_message="Password fields don't match")),
        Field('Nationality','string',requires=IS_NOT_EMPTY()),
		Field('Current_Affiliation','string',requires=IS_NOT_EMPTY()),
		Field('Status_of_Ph_D','string',requires=IS_IN_SET(['Completed', 'All but Dissertation', 'Not Completed'])),
		Field('Date_of_Completion','date',requires=IS_NOT_EMPTY()),
		Field('Institute_of_Ph_D','string',requires=IS_NOT_EMPTY()),
        Field('Cover_Letter','upload',requires=IS_NOT_EMPTY()),
		Field('Resume','upload',requires=IS_NOT_EMPTY()),
        Field('Status','string',readable=False,writable=False,default=30), #Nurendra: 0-Rejected 30-Applied 50-Applied with extra information 80-Shortlisted/Called for interview 90-Interview done/Awaiting Results 100-Accepted [Application can be rejected at any stage by makin Status=0]
		Field('Level','integer',readable=False,writable=False,default=0),#Nurendra: 0-User 1-Faculty 2-FacultyChamp 3-FacultyAdmin
        Field('Time','datetime',readable=False,writable=False,default=request.now)
]
#Nurendra: Defining the status level definitions
statusDict = { 0 : 'Rejected', 30 : 'Applied with Basic Information', 50 : 'Applied with extra information', 80 : 'Shortlisted- You will called for an interview shortly', 90 : 'Interview Done- Results will be declared soon', 100 : 'Accepted'}

auth.settings.extra_fields[auth.settings.table_user_name] = (register_extra_fields)
auth.define_tables(username=True, signature=False)
auth.settings.register_verify_password=False
auth.settings.formstyle="bootstrap"
auth.messages.verify_email = 'Click on the link http://hiring.iiit.ac.in/' +    URL(r=request,c='default',f='user',args=['verify_email']) +     '/%(key)s to verify your email'
auth.messages.reset_password = 'Click on the link http://hiring.iiit.ac.in' +     URL(r=request,c='default',f='user',args=['reset_password']) +     '/%(key)s to reset your password'

auth_table = db[auth.settings.table_user_name]
auth_table.Resume.requires = [IS_NOT_EMPTY()]
auth_table.AccessLevel = [IS_INT_IN_RANGE(0,4)]

#Nurendra: Configuring email for registration verification
mail = auth.settings.mailer
mail.settings.server = 'smtp.gmail.com:25' or 'logging'
mail.settings.sender = 'facultyhiringportal@gmail.com'
mail.settings.login = 'facultyhiringportal@gmail.com:facultyiiit123'
mail.settings.tls = True

#Nurendra: Configuring email for sending mails
from gluon.tools import Mail
mail2=Mail()
mail2.settings.server = 'smtp.gmail.com:25' or 'logging'
mail2.settings.sender = 'facultyhiringportal@gmail.com'
mail2.settings.login = 'facultyhiringportal@gmail.com:facultyiiit123'
mail2.settings.tls = True

## configure auth policy
auth.settings.registration_requires_verification = True
auth.settings.registration_requires_approval = False #sriram Admin doesnt need to approve
auth.settings.reset_password_requires_verification = True
#rauth.settings.registration_requires_verification = True
#rauth.settings.registration_requires_approval = False #sriram Admin doesnt need to approve
#rauth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.janrain_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#Nurendra: cannot use directly in field
value=0
if auth.is_logged_in():
    value=auth.user.username

#Nurendra: Optional Addon Table
db.define_table('optionalAddon',
                Field('Username','string',default=value,readable=True,writable=False),
                Field('Primary_Research_Area','string'),
                Field('Signal_Processing_and_Communications_Research_Center','boolean'),
                Field('Center_for_Data_Engineering','boolean'),
                Field('Language_Technologies_Research_Center','boolean'),
                Field('Robotics_Research_Center','boolean'),
                Field('Center_for_Security_Theory_and_Algorithms','boolean'),
                Field('Software_Engineering_Research_Center','boolean'),
                Field('Center_for_Visual_Information_Technology','boolean'),
                Field('Center_for_VLSI_and_Embedded_Systems_Technology','boolean'),
                Field('Research_Statement','upload',requires=IS_EMPTY_OR(IS_UPLOAD_FILENAME(extension='pdf'))),
                Field('Teaching_Statement','upload',requires=IS_EMPTY_OR(IS_UPLOAD_FILENAME(extension='pdf'))),
                Field('Relevant_Research_Paper_1','upload',requires=IS_EMPTY_OR(IS_UPLOAD_FILENAME(extension='pdf'))),
                Field('Relevant_Research_Paper_2','upload',requires=IS_EMPTY_OR(IS_UPLOAD_FILENAME(extension='pdf'))),
                Field('Relevant_Research_Paper_3','upload',requires=IS_EMPTY_OR(IS_UPLOAD_FILENAME(extension='pdf'))),
                Field('Recommendation_Name_1','string'),
                Field('Recommendation_Email_1','string'),
                Field('Recommendation_Name_2','string'),
                Field('Recommendation_Email_2','string'),
                Field('Recommendation_Name_3','string'),
                Field('Recommendation_Email_3','string'),
                Field('login_flag','boolean',default=False,readable=False,writable=False)
                )
#sriram recommendrs portal
db.define_table('recommenders_login',
		Field('email','string',requires=[IS_EMAIL()]),
		Field('passkey','string',requires=[IS_NOT_EMPTY()]),
		Field('login_count','boolean',default=False,readable=False,writable=False), #login count to limit numer of logins
		Field('recommendingfor','string',requires=[IS_NOT_EMPTY(),IS_IN_DB(db,auth_table.username)],readable=True,writable=False),
#user_name of recommendee
	       )

#Nurendra: Database for Recommendation Portal
db.define_table('recommendationPage',
                Field('Recommendation_forUsername','string',default='1',readable=False,writable=False),
                Field('Recommendation_For','string',default='1',readable=True,writable=False),
                Field('First_Name','string',requires=IS_NOT_EMPTY()),
                Field('Last_Name','string',requires=IS_NOT_EMPTY()),
                Field('Email','string',default='1',readable=False,writable=False),
                Field('Current_Affiliation','string',requires=IS_NOT_EMPTY()),
                Field('Current_Designation','string',requires=IS_NOT_EMPTY()),
                Field('Recommendation_Letter','upload',requires=[IS_NOT_EMPTY(),IS_UPLOAD_FILENAME(extension='pdf')])
                )
db.define_table('forwardedToAdmin',
                Field('applicationId','integer',readable=False,writable=False),
               )

db.define_table('roles',
                Field('Name','string',length=100,requires=IS_NOT_EMPTY()),
                Field('Designation','string',length=100,requires=IS_NOT_EMPTY()),
                Field('Email_Id','string',length=100,requires=[IS_NOT_EMPTY(),IS_EMAIL(),IS_NOT_IN_DB(db,'roles.Email_Id')],unique=True),
                Field('Role','string',length=100,requires=IS_IN_SET(['Administrator','Faculty Hiring Committee Chair','Faculty Hiring Committee Member','Faculty Member', 'Staff Member']))
                )
#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
