# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(B('IIIT Hyderabad'),
                  _class="brand",_href=URL('default','index'))
response.title = request.application.replace('_',' ').title()
response.subtitle = ''

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'SSAD Team 13 <you@example.com>'
response.meta.keywords = 'Faculty, Hiring, Portal, IIIT, employment, jobs, professor, faculty'
response.meta.generator = 'IIIT Faculty Hiring Portal'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T(''), False, URL(''), [])
]
response.faculty=[]

try:
    session.token
except:
    session.token = None

Email = 'Faculty Login'
if session.token !=None:
    Email = 'Welcome ' + session.token
DEVELOPMENT_MENU = True

#########################################################################
## provide shortcuts for development. remove in production
#########################################################################

def _():
    # shortcuts
    app = request.application
    ctr = request.controller
    if Email == 'Faculty Login':
        response.faculty += [
            (SPAN(Email, _class='highlighted'), False, URL(c = 'employee', f = 'index'),[
            (T('Login'), False, URL(c = 'employee', f = 'login'))
            ]),
            ]
    else:
        response.faculty += [
            (SPAN(Email, _class='highlighted'), False, URL(c = 'employee', f = 'index'),[
            (T('Home'), False, URL(c='employee', f = 'index')),
            (T('Logout'), False, URL(c = 'employee', f = 'logout'))
            ]),
            ]

if DEVELOPMENT_MENU: _()

if "auth" in locals(): auth.wikimenu()
