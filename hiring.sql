-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: hiring
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_cas`
--

DROP TABLE IF EXISTS `auth_cas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_cas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `service` varchar(512) DEFAULT NULL,
  `ticket` varchar(512) DEFAULT NULL,
  `renew` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id__idx` (`user_id`),
  CONSTRAINT `auth_cas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_cas`
--

LOCK TABLES `auth_cas` WRITE;
/*!40000 ALTER TABLE `auth_cas` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_cas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_event`
--

DROP TABLE IF EXISTS `auth_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` datetime DEFAULT NULL,
  `client_ip` varchar(512) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `origin` varchar(512) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`),
  KEY `user_id__idx` (`user_id`),
  CONSTRAINT `auth_event_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_event`
--

LOCK TABLES `auth_event` WRITE;
/*!40000 ALTER TABLE `auth_event` DISABLE KEYS */;
INSERT INTO `auth_event` VALUES (1,'2015-02-06 06:56:15','10.1.39.30',NULL,'auth','Group 1 created'),(2,'2015-02-06 06:56:15','10.1.39.30',NULL,'auth','User 1 Registered'),(3,'2015-02-06 06:56:58','10.1.39.30',NULL,'auth','User 1 Verification email sent'),(4,'2015-02-06 06:57:19','10.1.39.30',1,'auth','User 1 Logged-in'),(5,'2015-02-06 06:58:32','10.1.39.30',1,'auth','User 1 Logged-out'),(6,'2015-02-06 06:58:32','10.1.39.30',1,'auth','User 1 Logged-out'),(7,'2015-02-06 16:15:10','10.1.39.30',1,'auth','User 1 Logged-in'),(8,'2015-04-18 22:23:32','10.1.39.30',NULL,'auth','Group 2 created'),(9,'2015-04-18 22:23:32','10.1.39.30',NULL,'auth','User 2 Registered'),(10,'2015-04-18 22:23:53','10.1.39.30',NULL,'auth','User 2 Verification email sent'),(11,'2015-04-18 22:24:03','10.1.39.30',2,'auth','User 2 Logged-in'),(12,'2015-04-18 22:24:22','10.1.39.30',2,'auth','User 2 Logged-out'),(13,'2015-04-18 22:24:22','10.1.39.30',2,'auth','User 2 Logged-out');
/*!40000 ALTER TABLE `auth_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(512) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'user_1','Group uniquely assigned to user 1'),(2,'user_2','Group uniquely assigned to user 2');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_membership`
--

DROP TABLE IF EXISTS `auth_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_membership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id__idx` (`user_id`),
  KEY `group_id__idx` (`group_id`),
  CONSTRAINT `auth_membership_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `auth_membership_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_membership`
--

LOCK TABLES `auth_membership` WRITE;
/*!40000 ALTER TABLE `auth_membership` DISABLE KEYS */;
INSERT INTO `auth_membership` VALUES (1,1,1),(2,2,2);
/*!40000 ALTER TABLE `auth_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(512) DEFAULT NULL,
  `table_name` varchar(512) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id__idx` (`group_id`),
  CONSTRAINT `auth_permission_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `email` varchar(512) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  `registration_key` varchar(512) DEFAULT NULL,
  `reset_password_key` varchar(512) DEFAULT NULL,
  `registration_id` varchar(512) DEFAULT NULL,
  `Confirm_Password` varchar(512) DEFAULT NULL,
  `Nationality` varchar(512) DEFAULT NULL,
  `Current_Affiliation` varchar(512) DEFAULT NULL,
  `Status_of_Ph_D` varchar(512) DEFAULT NULL,
  `Date_of_Completion` date DEFAULT NULL,
  `Institute_of_Ph_D` varchar(512) DEFAULT NULL,
  `Cover_Letter` varchar(512) DEFAULT NULL,
  `Resume` varchar(512) DEFAULT NULL,
  `Status` varchar(512) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'Dane','Halls','nurendrachoudhary31@gmail.com','dane','pbkdf2(1000,20,sha512)$b8adccb4db44dfdb$fb9a4f1bceb5bd00f5e6162232aca6788522d7e8','','','','iiit123','Indian','IIIT','All but Dissertation','2015-02-07','IIIT','auth_user.Cover_Letter.92f1534c3c151c46.612e706466.pdf','auth_user.Resume.b5950d6502b70719.612e706466.pdf','30',0,'2015-02-06 06:56:15'),(2,'Adam','Christ','jawatiwug@landmail.co','adam','pbkdf2(1000,20,sha512)$bc5e3e29a6c609cb$6d5dbb20d5a918448bdf98e4554aa29626609ff4','','','','iiit123','Indian','IIIT','All but Dissertation','2015-04-29','IIIT','auth_user.Cover_Letter.af294917ada955af.696d706f7274616e742e747874.txt','auth_user.Resume.8a7b219e5aecf822.6d616e32.txt','30',0,'2015-04-18 22:23:32');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forwardedToAdmin`
--

DROP TABLE IF EXISTS `forwardedToAdmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forwardedToAdmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forwardedToAdmin`
--

LOCK TABLES `forwardedToAdmin` WRITE;
/*!40000 ALTER TABLE `forwardedToAdmin` DISABLE KEYS */;
INSERT INTO `forwardedToAdmin` VALUES (1,1);
/*!40000 ALTER TABLE `forwardedToAdmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optionalAddon`
--

DROP TABLE IF EXISTS `optionalAddon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optionalAddon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(512) DEFAULT NULL,
  `Primary_Research_Area` varchar(512) DEFAULT NULL,
  `Signal_Processing_and_Communications_Research_Center` char(1) DEFAULT NULL,
  `Center_for_Data_Engineering` char(1) DEFAULT NULL,
  `Language_Technologies_Research_Center` char(1) DEFAULT NULL,
  `Robotics_Research_Center` char(1) DEFAULT NULL,
  `Center_for_Security_Theory_and_Algorithms` char(1) DEFAULT NULL,
  `Software_Engineering_Research_Center` char(1) DEFAULT NULL,
  `Center_for_Visual_Information_Technology` char(1) DEFAULT NULL,
  `Center_for_VLSI_and_Embedded_Systems_Technology` char(1) DEFAULT NULL,
  `Research_Statement` varchar(512) DEFAULT NULL,
  `Teaching_Statement` varchar(512) DEFAULT NULL,
  `Relevant_Research_Paper_1` varchar(512) DEFAULT NULL,
  `Relevant_Research_Paper_2` varchar(512) DEFAULT NULL,
  `Relevant_Research_Paper_3` varchar(512) DEFAULT NULL,
  `Recommendation_Name_1` varchar(512) DEFAULT NULL,
  `Recommendation_Email_1` varchar(512) DEFAULT NULL,
  `Recommendation_Name_2` varchar(512) DEFAULT NULL,
  `Recommendation_Email_2` varchar(512) DEFAULT NULL,
  `Recommendation_Name_3` varchar(512) DEFAULT NULL,
  `Recommendation_Email_3` varchar(512) DEFAULT NULL,
  `login_flag` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optionalAddon`
--

LOCK TABLES `optionalAddon` WRITE;
/*!40000 ALTER TABLE `optionalAddon` DISABLE KEYS */;
/*!40000 ALTER TABLE `optionalAddon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommendationPage`
--

DROP TABLE IF EXISTS `recommendationPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendationPage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Recommendation_forUsername` varchar(512) DEFAULT NULL,
  `Recommendation_For` varchar(512) DEFAULT NULL,
  `First_Name` varchar(512) DEFAULT NULL,
  `Last_Name` varchar(512) DEFAULT NULL,
  `Email` varchar(512) DEFAULT NULL,
  `Current_Affiliation` varchar(512) DEFAULT NULL,
  `Current_Designation` varchar(512) DEFAULT NULL,
  `Recommendation_Letter` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommendationPage`
--

LOCK TABLES `recommendationPage` WRITE;
/*!40000 ALTER TABLE `recommendationPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendationPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommenders_login`
--

DROP TABLE IF EXISTS `recommenders_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommenders_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(512) DEFAULT NULL,
  `passkey` varchar(512) DEFAULT NULL,
  `login_count` char(1) DEFAULT NULL,
  `recommendingfor` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommenders_login`
--

LOCK TABLES `recommenders_login` WRITE;
/*!40000 ALTER TABLE `recommenders_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommenders_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Designation` varchar(100) DEFAULT NULL,
  `Email_Id` varchar(100) DEFAULT NULL,
  `Role` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Email_Id` (`Email_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-18 23:13:19
